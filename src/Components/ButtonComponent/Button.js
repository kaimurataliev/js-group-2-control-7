/**
 * Created by kaito on 27.01.2018.
 */
import React from 'react';
import './ButtonStyle.css';

const Button = (props) => {
    return (
        <button onClick={props.addOrder} className="button">{props.children}</button>
    )
};

export default Button;