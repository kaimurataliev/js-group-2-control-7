import React from 'react';
import Button from '../ButtonComponent/Button';
import './ButtonPlaceStyle.css';

let buttons = [
    {type: 'Burger', price: 80},
    {type: 'Samsa', price: 50},
    {type: 'Fries', price: 40},
    {type: 'Coffee', price: 70},
    {type: 'Tea', price: 50},
    {type: 'Cola', price: 40}
];

const ButtonPlace = (props) => {
    return (
        <div className="ButtonsPlace">
            <h2>Please choose your meal</h2>
            {buttons.map((key, index) => {
                return  <Button total={() => {props.getTotal}} addOrder={() => props.addOrder(index)} key={index}>
                    <p>{key.type}</p>
                    <p>{key.price} som</p>
                </Button>
            })}
        </div>
    )
};

export default ButtonPlace;