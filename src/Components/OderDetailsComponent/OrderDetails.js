import React from 'react';
import './OrderDetailsStyle.css';

const hideText = (props) => {
    let startText = true;
    props.orders.forEach((key) => {
        !key.choose ? startText : startText = false;
    });
    if(startText) {
        return <span className="startText">Your list is empty. Please add something.</span>
    }
};

const getTotal = (props) => {
    let total = 0;
    props.orders.forEach((key) => {
        total = total + key.price * key.amount;
    });
    return <span className="getTotal">{total} som</span>
};


const showOrders = (value, index, props) => {
    if(value.choose) {
        return (
            <div key={index} className="order">
                <span>{value.type} </span>
                <span>x: {value.amount} </span>
                <span className="price">price: {value.amount * value.price}</span>
                <button onClick={() => {props.deleteOrder(index)}} className="deleteOrder">X</button>
            </div>
        )
    }
};

const OrderDetails = (props) => {
    const orders = props.orders;

    return (
        <div className="Details">
            <div className="item">
                {hideText(props)}
                {orders.map((item, index) => {
                    return showOrders(item, index, props)
                })}
            </div>
            <div className="totalPrice">
                total price: {getTotal(props)}
            </div>
        </div>
    )
};

export default OrderDetails;
