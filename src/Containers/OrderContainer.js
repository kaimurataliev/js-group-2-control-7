import React, { Component } from 'react';
import ButtonPlace from '../Components/ButtonsPlace/ButtonPlace';
import OrderDetails from '../Components/OderDetailsComponent/OrderDetails';
import './OrderContainerStyle.css'

export default class OrderContainer extends Component {

    state = {
        orders: [
            {type: 'Burger', price: 80, amount: 0, choose: false},
            {type: 'Samsa', price: 50, amount: 0, choose: false},
            {type: 'Fries', price: 40, amount: 0, choose: false},
            {type: 'Coffee', price: 70, amount: 0, choose: false},
            {type: 'Tea', price: 50, amount: 0, choose: false},
            {type: 'Cola', price: 40, amount: 0, choose: false}
        ]
    };

    addOrder = (index) => {
        let orders = [...this.state.orders];
        orders[index].choose = true;
        orders[index].amount++;
        this.setState({orders, startText:false});
    };

    deleteOrder = (index) => {
        let orders = [...this.state.orders];
        orders[index].choose = false;
        orders[index].amount = 0;
        this.setState({orders, startText:false});
    };




    render() {
        return(
            <div className="OrderContainer">
                <div>
                    <OrderDetails total={() => this.getTotal()} deleteOrder={this.deleteOrder} orders={this.state.orders} />
                </div>
                <div>
                    <ButtonPlace addOrder={this.addOrder}/>
                </div>
            </div>
        )
    }
};