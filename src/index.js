import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import OrderContainer from './Containers/OrderContainer';

ReactDOM.render(<OrderContainer />, document.getElementById('root'));
registerServiceWorker();
